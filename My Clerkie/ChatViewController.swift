//
//  ChatViewController.swift
//  My Clerkie
//
//  Created by Jeyashri Natarajan on 11/19/17.
//  Copyright © 2017 Clerkie. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController,UIScrollViewDelegate, UITextViewDelegate {
    
    @IBOutlet var resultsScrollView: UIScrollView!
    @IBOutlet var frameMessageView: UIView!
    @IBOutlet var lineLbl: UILabel!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var sendBtn: UIButton!
    
    var scrollViewOriginalY:CGFloat = 0
    var frameMessageOriginalY:CGFloat = 0
    
    let mLbl = UILabel(frame: CGRect(x:5,y: 10,width: 200, height:20))
    
    var messageX:CGFloat = 37.0
    var messageY:CGFloat = 26.0
    var frameX:CGFloat = 32.0
    var frameY:CGFloat = 21.0
    
    var senderMessageArray = [String]()
    var aiMessageArray = [String]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        scrollViewOriginalY = self.resultsScrollView.frame.origin.y
        frameMessageOriginalY = self.frameMessageView.frame.origin.y
        
        mLbl.text = "Type a message..."
        mLbl.backgroundColor = UIColor.clear
        mLbl.textColor = UIColor.lightGray
        messageTextView.addSubview(mLbl)
        
        aiMessageArray.append("Hi")
        aiMessageArray.append("How can I help you?")
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
 
        let tapScrollViewGesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView))
        tapScrollViewGesture.numberOfTapsRequired = 1
        resultsScrollView.addGestureRecognizer(tapScrollViewGesture)
        
        refreshResults()
        
    }
   @objc func didTapScrollView() {
        
        self.view.endEditing(true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if !messageTextView.hasText{
            
            self.mLbl.isHidden = false
        } else {
            
            self.mLbl.isHidden = true
        }
        
    }
    
  @objc func textViewDidEndEditing(_ textView: UITextView) {
        
        if !messageTextView.hasText {
            self.mLbl.isHidden = false
        }
    }
    
  @objc func keyboardWasShown(notification:NSNotification) {
        
    let dict:NSDictionary = notification.userInfo! as NSDictionary
        let s:NSValue = dict.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
    let rect:CGRect = s.cgRectValue
        
    UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            
            self.resultsScrollView.frame.origin.y = self.scrollViewOriginalY - rect.height
            self.frameMessageView.frame.origin.y = self.frameMessageOriginalY - rect.height
            
            let bottomOffset:CGPoint = CGPoint(x:0, y:self.resultsScrollView.contentSize.height - self.resultsScrollView.bounds.size.height)
            self.resultsScrollView.setContentOffset(bottomOffset, animated: false)
            
        }, completion: {
            (finished:Bool) in
            
        })
        
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: {
            
            self.resultsScrollView.frame.origin.y = self.scrollViewOriginalY
            self.frameMessageView.frame.origin.y = self.frameMessageOriginalY
            
            let bottomOffset:CGPoint = CGPoint(x:0, y:self.resultsScrollView.contentSize.height - self.resultsScrollView.bounds.size.height)
            self.resultsScrollView.setContentOffset(bottomOffset, animated: false)
            
        }, completion: {
            (finished:Bool) in
            
        })
        
        
    }
    
    func refreshResults(){
         let width = view.frame.size.width
        messageX = 37.0
        messageY = 26.0
        frameX = 32.0
        frameY = 21.0
        if aiMessageArray.count>0{
            for i in 0..<aiMessageArray.count{
                let messageLbl = CopyableLabel()
                messageLbl.frame = CGRect(x:0,y: 0, width:self.resultsScrollView.frame.size.width-94,height: CGFloat.greatestFiniteMagnitude)
                messageLbl.backgroundColor = UIColor.groupTableViewBackground
                messageLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
                messageLbl.textAlignment = NSTextAlignment.left
                messageLbl.numberOfLines = 0
                messageLbl.font = UIFont(name: "Helvetica Neuse", size: 17)
                messageLbl.textColor = UIColor.black
                messageLbl.text = self.aiMessageArray[i]
                messageLbl.sizeToFit()
                messageLbl.layer.zPosition = 20
                messageLbl.frame.origin.x = self.messageX
                messageLbl.frame.origin.y = self.messageY
                self.resultsScrollView.addSubview(messageLbl)
                self.messageY += messageLbl.frame.size.height + 30
                let frameLbl:UILabel = UILabel()
                frameLbl.frame = CGRect(x:self.frameX, y:self.frameY, width:messageLbl.frame.size.width+10, height:messageLbl.frame.size.height+10)
                frameLbl.backgroundColor = UIColor.groupTableViewBackground
                frameLbl.layer.masksToBounds = true
                frameLbl.layer.cornerRadius = 10
                self.resultsScrollView.addSubview(frameLbl)
                self.frameY += frameLbl.frame.size.height + 20
                self.resultsScrollView.contentSize = CGSize(width:width,height: self.messageY)
            }
        }
        if senderMessageArray.count > 0{
            for i in 0..<senderMessageArray.count{
                let messageLbl = CopyableLabel()
                messageLbl.frame = CGRect(x:0,y: 0, width:self.resultsScrollView.frame.size.width-94,height: CGFloat.greatestFiniteMagnitude)
                messageLbl.backgroundColor = UIColor(red:3/255.0, green:188/255.0, blue:229/255.0, alpha:1.0)
                messageLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
                messageLbl.textAlignment = NSTextAlignment.left
                messageLbl.numberOfLines = 0
                messageLbl.font = UIFont(name: "Helvetica Neuse", size: 17)
                messageLbl.textColor = UIColor.white
                messageLbl.text = self.senderMessageArray[i]
                messageLbl.sizeToFit()
                messageLbl.layer.zPosition = 20
                messageLbl.frame.origin.x = (self.resultsScrollView.frame.size.width - self.messageX) - messageLbl.frame.size.width
                messageLbl.frame.origin.y = self.messageY
                self.resultsScrollView.addSubview(messageLbl)
                self.messageY += messageLbl.frame.size.height + 30
                let frameLbl:UILabel = UILabel()
                frameLbl.frame.size = CGSize(width:messageLbl.frame.size.width+10, height:messageLbl.frame.size.height+10)
                frameLbl.frame.origin.x = (self.resultsScrollView.frame.size.width - self.frameX) - frameLbl.frame.size.width
                frameLbl.frame.origin.y = self.frameY
                frameLbl.backgroundColor = UIColor(red:3/255.0, green:188/255.0, blue:229/255.0, alpha:1.0)
                frameLbl.layer.masksToBounds = true
                frameLbl.layer.cornerRadius = 10
                self.resultsScrollView.addSubview(frameLbl)
                self.frameY += frameLbl.frame.size.height + 20
                self.resultsScrollView.contentSize = CGSize(width:width, height:self.messageY)
            }
        }
        
    
       
        let bottomOffset:CGPoint = CGPoint(x:0, y:self.resultsScrollView.contentSize.height - self.resultsScrollView.bounds.size.height)
        self.resultsScrollView.setContentOffset(bottomOffset, animated: false)
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        senderMessageArray.append(messageTextView.text)
        self.mLbl.isHidden = false
        self.messageTextView.text = ""
        refreshResults()
    }
    
}
